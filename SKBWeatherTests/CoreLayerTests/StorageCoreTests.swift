//
//  StorageCoreTests.swift
//  SKBWeatherTests
//
//  Created by Sergey Borovikov on 13/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import XCTest
import RealmSwift
@testable import SKBWeather

class StorageCoreTests: XCTestCase {
    
    private var storageMock: StorageCore!
    private var realm: Realm!
    
    override func setUp() {
        super.setUp()
        realm = RealmProvider.realm()
        storageMock = StorageCore(realm: realm)
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testSaveWeatherModel() {
        storageMock.save(getWeatherModelTest())
        let results = realm.objects(WeatherRealm.self)
        XCTAssert(results.count > 0)
        XCTAssert(results.first?.id == 0)
        XCTAssert(results.first?.city == "city")
        XCTAssert(results.first?.descript == "des")
        XCTAssert(results.first?.icon == IconType._01d.rawValue)
    }
    func testGetAllWeatherModels() {
        let testWeatherModel = getWeatherModelTest()
        storageMock.save(testWeatherModel)
        let storedTestWeatherModel = storageMock.getAllWeatherModels().first
        XCTAssert(storedTestWeatherModel != nil)
        XCTAssert(testWeatherModel.id == storedTestWeatherModel?.id)
        XCTAssert(testWeatherModel.city == storedTestWeatherModel?.city)
        XCTAssert(testWeatherModel.descript == storedTestWeatherModel?.descript)
        XCTAssert(testWeatherModel.date == storedTestWeatherModel?.date)
        XCTAssert(testWeatherModel.icon == storedTestWeatherModel?.icon)
        XCTAssert(testWeatherModel.temp == storedTestWeatherModel?.temp)
        XCTAssert(testWeatherModel.tempMax == storedTestWeatherModel?.tempMax)
        XCTAssert(testWeatherModel.tempMin == storedTestWeatherModel?.tempMin)
    }
    func testDeleteLastWetherModel() {
        let testWeatherModel = getWeatherModelTest()
        storageMock.save(testWeatherModel)
        storageMock.deleteLastWetherModel()
        XCTAssert(storageMock.getAllWeatherModels().count == 0)
    }
    
    private func getWeatherModelTest() -> WeatherModel {
        let result = WeatherModel(id: 0,
                                  city: "city",
                                  descript: "des",
                                  date: Date(),
                                  icon: IconType._01d,
                                  temp: 0,
                                  tempMax: 0,
                                  tempMin: 0)
        return result
    }
}
