//
//  WebClientCoreMock.swift
//  SKBWeatherTests
//
//  Created by Sergey Borovikov on 13/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import Foundation

import Foundation
@testable import SKBWeather

final class WebClientCoreMock {}

extension WebClientCoreMock: WebClientCoreI {
    
    func sendRequest(request: WebClientRequestModel, success: @escaping (Any) -> Void, failure: @escaping (WebClientError) -> Void) {
        
        if request.httpResurcePath == Constants.WebClient.FORECAST_URL &&
            request.methodType == .get {
            success([self.getTestWeatherDic()])
            
        } else if request.httpResurcePath == Constants.WebClient.WEATHER_URL &&
            request.methodType == .get {
            success(self.getTestWeatherDic())
            
        } else {
            success(request.httpResurcePath as AnyObject)
        }
    }
    
    private func getTestWeatherDic() -> [String: Any] {
        
        let testResult = ["id": 0,
                          "city": "",
                          "descript": "",
                          "date" : Date(),
                          "icon" : IconType._01d,
                          "temp" : 0,
                          "tempMax" : 0,
                          "tempMin" : 0] as [String: Any]
        
        return testResult
    }
}
