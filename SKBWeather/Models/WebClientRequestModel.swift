//
//  WebClientRequestModel.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import Foundation

enum WebClientHTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

struct WebClientRequestModel {
    let methodType: WebClientHTTPMethod
    let parames: [String: Any]?
    let mediaFile: Data?
    let httpResurcePath: String
}
