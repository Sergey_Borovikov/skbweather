//
//  MapperCoreTests.swift
//  SKBWeatherTests
//
//  Created by Sergey Borovikov on 13/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import XCTest
import GooglePlaces
@testable import SKBWeather

class MapperCoreTests: XCTestCase {
    
    private var mapper: MapperCoreI!
    
    private let ID = 0
    private let NAME = "city"
    private let DESCRIPT = "des"
    private let ICON = "01d"
    private let TEMP_K = 283.15
    private let TEMP_C = 10
    private let DATE = "2018-07-10 09:15:20"
    
    override func setUp() {
        super.setUp()
        mapper = MapperCore()
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testMapCityWeather() {
        let testWeatherDic = getTestWeatherDic()
        let mappedWeatherModel = mapper.mapCityWeather(testWeatherDic)
        XCTAssert(mappedWeatherModel?.id == ID)
        XCTAssert(mappedWeatherModel?.descript == DESCRIPT)
        XCTAssert(mappedWeatherModel?.icon.rawValue == ICON)
        XCTAssert(mappedWeatherModel?.city == NAME)
        XCTAssert(mappedWeatherModel?.temp == TEMP_C)
    }
    
    func testMapForecast() {
        let testForecast = ["list" : [getTestWeatherDic()]]
        let mappedForecast = mapper.mapForecast(testForecast)
        XCTAssert(mappedForecast.count > 0)
        XCTAssert(mappedForecast.first?.id! == ID)
        XCTAssert(mappedForecast.first!.icon.rawValue == ICON)
        XCTAssert(mappedForecast.first?.tempMax == TEMP_C)
        XCTAssert(mappedForecast.first?.tempMin == TEMP_C)
    }
    
    private func getTestWeatherDic() -> [String: Any] {
        let result = ["weather": [["id" : ID], ["description": DESCRIPT], ["icon" : ICON]],
                      "name" : NAME,
                      "main" : ["temp" : TEMP_K, "temp_min" : TEMP_K, "temp_max" : TEMP_K],
                      "dt_txt" : DATE] as [String: Any]
        return result
    }
}
