//
//  SwinjectStoryboard+Setup.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import SwinjectStoryboard

extension SwinjectStoryboard {
    
    @objc class func setup() {
        
        // CoreComponents
        defaultContainer.register(WebClientCoreI.self) { _ in WebClientCore(baseUrl: Constants.WebClient.OPEN_WEATHER_URL) }
        defaultContainer.register(RequestCoreI.self) { _ in RequestCore() }
        defaultContainer.register(MapperCoreI.self) { _ in MapperCore() }
        defaultContainer.register(StorageCoreI.self) { _ in StorageCore(realm: RealmProvider.realm()) }
        defaultContainer.register(NotificationCoreI.self) { _ in NotificationCore() }
        
        // Services
        defaultContainer.register(WeatherServiceI.self) { r in
            WeatherService(webClient: r.resolve(WebClientCoreI.self),
                           mapper: r.resolve(MapperCoreI.self),
                           storage: r.resolve(StorageCoreI.self)!,
                           reqests: r.resolve(RequestCoreI.self)!,
                           notifications: r.resolve(NotificationCoreI.self)!)
        }
        defaultContainer.register(PlacesServiceI.self) { r in
            PlacesService(mapper: r.resolve(MapperCoreI.self)!)
        }
        
        // Routers
        defaultContainer.register(StartRouterI.self) { _ in StartRouter() }
        defaultContainer.register(MainScreenRouterI.self) { _ in MainScreenRouter() }
        
        // ViewMdoels
        defaultContainer.register(MainScreenViewModelI.self) { r in
            MainScreenViewModel(weatherService: r.resolve(WeatherServiceI.self)!,
                                placesService: r.resolve(PlacesServiceI.self)!)
        }
        defaultContainer.register(WeatherHistoryViewModelI.self) { r in
            WeatherHistoryViewModel(weatherService: r.resolve(WeatherServiceI.self)!)
        }
        
        // ViewControllers
        defaultContainer.storyboardInitCompleted(StartVC.self) { r, c in
            c.router = r.resolve(StartRouterI.self)
        }
        defaultContainer.storyboardInitCompleted(MainScreenVC.self) { r, c in
            c.viewModel = r.resolve(MainScreenViewModelI.self)
            c.router = r.resolve(MainScreenRouterI.self)
        }
        defaultContainer.storyboardInitCompleted(WeatherHistoryVC.self) { r, c in
            c.viewModel = r.resolve(WeatherHistoryViewModelI.self)
        }
    }
}

