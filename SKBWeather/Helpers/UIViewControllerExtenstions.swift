//
//  UIViewControllerExtenstions.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import UIKit

extension UIViewController {
    func showActionSheet(title: String, msg: String) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showActionSheet(title: String, msg: String, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

