//
//  WeatherModel.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import UIKit

struct WeatherModel {
    let id: Int?
    let city: String?
    let descript: String?
    let date: Date
    let icon: IconType
    let temp: Int?
    let tempMax: Int?
    let tempMin: Int?
}

enum IconType: String {
    case none = ""
    case _01d = "01d"
    case _02d = "02d"
    case _03d = "03d"
    case _04d = "04d"
    case _09d = "09d"
    case _10d = "10d"
    case _11d = "11d"
    case _13d = "13d"
    case _50d = "50d"
    case _01n = "01n"
    case _02n = "02n"
    case _03n = "03n"
    case _04n = "04n"
    case _09n = "09n"
    case _10n = "10n"
    case _11n = "11n"
    case _13n = "13n"
    case _50n = "50n"
    
    func icon() -> UIImage {
        switch self {
        case .none:
            return UIImage()
        case ._01d, ._01n:
            return R.image.sunIcon()!
        case ._02d, ._02n:
            return R.image.cloudIcon()!
        case ._03d, ._03n:
            return R.image.cloudIcon()!
        case ._04d, ._04n:
            return R.image.doubleCloudIcon()!
        case ._09d, ._09n:
            return R.image.cloudRainIcon()!
        case ._10d, ._10n:
            return R.image.cloudSunRainIcon()!
        case ._11d, ._11n:
            return R.image.cloudLightingIcon()!
        case ._13d, ._13n:
            return R.image.snowIcon()!
        case ._50d, ._50n:
            return R.image.fogIcon()!
        }
    }
}

