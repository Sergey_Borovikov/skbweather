//
//  Constants.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import UIKit

struct Constants {
    
    static let DAY_WEATHER_CELL_HIGHT: CGFloat = 44
    
    struct GooglePlaces {
        static let API_KEY = "AIzaSyB9aWgGFB79feTnSCt3EIMNM9pRzt3pm84"
    }
    
    struct OpenWeaher {
        static let API_KEY = "db7c00a8009104145c61a304b1db2da7"
    }
    
    struct Animation {
        static let ANIMATION_TIME: TimeInterval = 0.3
        static let ANIMATION_SHORT_TIME: TimeInterval = 0.1
        static let ANIMATION_LONG_TIME: TimeInterval = 0.6
    }
    
    struct Storyboard {
        static let START_SB = "Start"
        static let MAIN_SCREEN_SB = "MainScreen"
        static let WEATHER_HISTORY_SB = "WeatherHistory"
    }
    
    struct ViewControllerID {
        static let START_VC = "StartVC"
        static let MAIN_SCREEN_VC = "MainScreenVC"
        static let WEATHER_HISTORY_VC = "WeatherHistoryVC"
    }
    
    struct WebClient {
        
        // URLs
        static let OPEN_WEATHER_URL = "http://api.openweathermap.org/data/2.5"
        static let WEATHER_URL = "/weather"
        static let FORECAST_URL = "/forecast"
        
        // Errors
        static let AUTHORIZATION_ERROR = NSLocalizedString("Authorization error", comment: "AUTHORIZATION_ERROR")
        static let CONNECTION_LOST_ERROR = NSLocalizedString("Connection lostadd", comment: "CONNECTION_LOST_ERROR")
        static let INCORRECT_DATA_RETURNED = NSLocalizedString("Incorrect data returned", comment: "INCORRECT_DATA_RETURNED")
        static let INTERNATIONAL_ROAMING_OFF = NSLocalizedString("International roaming off", comment: "INTERNATIONAL_ROAMING_OFF")
        static let NOT_CONNECTED_TO_INTERNET = NSLocalizedString("Not connected to internet", comment: "NOT_CONNECTED_TO_INTERNET")
        static let NOT_REACHED_SERVER = NSLocalizedString("Not reached server", comment: "NOT_REACHED_SERVER")
        static let NO_SUCH_ACCOUNT = NSLocalizedString("There is no such account", comment: "NO_SUCH_ACCOUNT")
        static let INTERNAL_SERVER_ERROR = NSLocalizedString("Internal server error", comment: "INTERNAL_SERVER_ERROR")
        static let UNKNOWN = NSLocalizedString("Unknown", comment: "UNKNOWN")
        static let CONFLICT = NSLocalizedString("Conflict", comment: "CONFLICT")
        static let BED_REQUEST = NSLocalizedString("Bed request", comment: "BED_REQUEST")
        static let NOT_FOUND = NSLocalizedString("Not found", comment: "NOT_FOUND")
        static let PAYLOAD_TOO_LARGE = NSLocalizedString("Payload too large", comment: "PAYLOAD_TOO_LARGE")
    }
}

