//
//  WeatherHistoryCell.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import UIKit

class WeatherHistoryCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLB: UILabel!
    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var degressLB: UILabel!
    
    private let df = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        df.dateFormat = "dd MMM"
        setupUI()
    }
    
    func set(weatherModel: WeatherModel) {
        cityNameLB.text = weatherModel.city
        dateLB.text = df.string(from: weatherModel.date)
        iconIV.image = weatherModel.icon.icon()
        degressLB.text = "\(weatherModel.temp ?? 0)"
    }
    
    private func setupUI() {
        iconIV.tintColor = R.color.blue()
    }
}
