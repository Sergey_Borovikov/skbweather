//
//  DayWeatherCell.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import UIKit

class DayWeatherCell: UITableViewCell {
    
    @IBOutlet weak var dayLB: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var dayDegreesLB: UILabel!
    @IBOutlet weak var nightDegressLB: UILabel!
    
    private let df = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        df.dateStyle = .long
        setupUI()
    }
    
    func set(dayWeatherModel: WeatherModel) {
        dayLB.text = df.string(from: dayWeatherModel.date)
        iconIV.image = dayWeatherModel.icon.icon()
        dayDegreesLB.text = "\(dayWeatherModel.tempMax ?? 0)"
        nightDegressLB.text = "\(dayWeatherModel.tempMin ?? 0)"
    }
    
    private func setupUI() {
        iconIV.tintColor = R.color.blue()
    }
}
