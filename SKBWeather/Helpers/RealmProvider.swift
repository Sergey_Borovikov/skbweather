//
//  RealmProvider.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import RealmSwift

class RealmProvider {
    
    class func realm() -> Realm {
        if let _ = NSClassFromString("XCTest") {
            return RealmProvider.inMemoryRealm()
        } else {
            return try! Realm();
        }
    }
    
    class func inMemoryRealm() -> Realm {
        return try! Realm(configuration: Realm.Configuration(fileURL: nil,
                                                             inMemoryIdentifier: "test",
                                                             encryptionKey: nil,
                                                             readOnly: false,
                                                             schemaVersion: 0,
                                                             migrationBlock: nil,
                                                             objectTypes: nil))
    }
}
