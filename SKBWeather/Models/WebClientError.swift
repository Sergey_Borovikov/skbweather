//
//  WebClientError.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import Foundation

public enum WebClientError: Error, CustomStringConvertible {
    /// Unknown or not supported error.
    case Unknown
    
    /// Not connected to the internet.
    case NotConnectedToInternet
    
    /// International data roaming turned off.
    case InternationalRoamingOff
    
    /// Cannot reach the server.
    case NotReachedServer
    
    /// Connection is lost.
    case ConnectionLost
    
    /// Incorrect data returned from the server.
    case IncorrectDataReturned
    
    /// AuthenticationRequired
    case AuthenticationRequired
    
    /// Unauthorized
    case Unauthorized
    
    /// no such account
    case NoSuchAccount
    
    /// internal server error
    case InternalServerError
    
    /// Conflict
    case Conflict
    
    /// Bed Request
    case BedRequest
    
    /// Not found
    case NotFound
    
    /// Other Errors
    case ErrorMsg(String)
    
    /// Payload Too Large
    case PayloadTooLarge
    
    internal init(code:Int) {
        switch(code) {
        case 400:
            self = .BedRequest
        case 401:
            self = .Unauthorized
        case 404:
            self = .NotFound
        case 409:
            self = .Conflict
        case 413:
            self = .PayloadTooLarge
        case 500:
            self = .InternalServerError
        default:
            self = .Unknown
        }
    }
    
    internal init(error: NSError) {
        if error.domain == NSURLErrorDomain {
            switch error.code {
            case NSURLErrorUnknown:
                self = .Unknown
            case NSURLErrorCancelled:
                self = .Unknown // Cancellation is not used in this project.
            case NSURLErrorBadURL:
                self = .IncorrectDataReturned // Because it is caused by a bad URL returned in a JSON response from the server.
            case NSURLErrorTimedOut:
                self = .NotReachedServer
            case NSURLErrorUnsupportedURL:
                self = .IncorrectDataReturned
            case NSURLErrorCannotFindHost, NSURLErrorCannotConnectToHost:
                self = .NotReachedServer
            case NSURLErrorDataLengthExceedsMaximum:
                self = .IncorrectDataReturned
            case NSURLErrorNetworkConnectionLost:
                self = .ConnectionLost
            case NSURLErrorDNSLookupFailed:
                self = .NotReachedServer
            case NSURLErrorHTTPTooManyRedirects:
                self = .Unknown
            case NSURLErrorResourceUnavailable:
                self = .IncorrectDataReturned
            case NSURLErrorNotConnectedToInternet:
                self = .NotConnectedToInternet
            case NSURLErrorRedirectToNonExistentLocation, NSURLErrorBadServerResponse:
                self = .IncorrectDataReturned
            case NSURLErrorUserCancelledAuthentication, NSURLErrorUserAuthenticationRequired:
                self = .AuthenticationRequired
            case NSURLErrorZeroByteResource, NSURLErrorCannotDecodeRawData, NSURLErrorCannotDecodeContentData:
                self = .IncorrectDataReturned
            case NSURLErrorCannotParseResponse:
                self = .IncorrectDataReturned
            case NSURLErrorInternationalRoamingOff:
                self = .InternationalRoamingOff
            case NSURLErrorCallIsActive, NSURLErrorDataNotAllowed, NSURLErrorRequestBodyStreamExhausted:
                self = .Unknown
            case NSURLErrorFileDoesNotExist, NSURLErrorFileIsDirectory:
                self = .IncorrectDataReturned
            case
            NSURLErrorNoPermissionsToReadFile,
            NSURLErrorSecureConnectionFailed,
            NSURLErrorServerCertificateHasBadDate,
            NSURLErrorServerCertificateUntrusted,
            NSURLErrorServerCertificateHasUnknownRoot,
            NSURLErrorServerCertificateNotYetValid,
            NSURLErrorClientCertificateRejected,
            NSURLErrorClientCertificateRequired,
            NSURLErrorCannotLoadFromNetwork,
            NSURLErrorCannotCreateFile,
            NSURLErrorCannotOpenFile,
            NSURLErrorCannotCloseFile,
            NSURLErrorCannotWriteToFile,
            NSURLErrorCannotRemoveFile,
            NSURLErrorCannotMoveFile,
            NSURLErrorDownloadDecodingFailedMidStream,
            NSURLErrorDownloadDecodingFailedToComplete:
                self = .Unknown
            default:
                self = .Unknown
            }
        }
        else {
            self = .Unknown
        }
    }
    
    public var description: String {
        let text: String
        switch self {
        case .Unknown:
            text = Constants.WebClient.UNKNOWN
        case .NotConnectedToInternet:
            text = Constants.WebClient.NOT_CONNECTED_TO_INTERNET
        case .InternationalRoamingOff:
            text = Constants.WebClient.INTERNATIONAL_ROAMING_OFF
        case .NotReachedServer:
            text = Constants.WebClient.NOT_REACHED_SERVER
        case .ConnectionLost:
            text = Constants.WebClient.CONNECTION_LOST_ERROR
        case .IncorrectDataReturned:
            text = Constants.WebClient.INCORRECT_DATA_RETURNED
        case .AuthenticationRequired:
            text = Constants.WebClient.AUTHORIZATION_ERROR
        case .NoSuchAccount:
            text = Constants.WebClient.NO_SUCH_ACCOUNT
        case .InternalServerError:
            text = Constants.WebClient.INTERNAL_SERVER_ERROR
        case .Conflict:
            text = Constants.WebClient.CONFLICT
        case .BedRequest:
            text = Constants.WebClient.BED_REQUEST
        case .NotFound:
            text = Constants.WebClient.NOT_FOUND
        case .Unauthorized:
            text = Constants.WebClient.AUTHORIZATION_ERROR
        case .PayloadTooLarge:
            text = Constants.WebClient.PAYLOAD_TOO_LARGE
        case .ErrorMsg(let msg):
            text = msg
        }
        return text
    }
}

