//
//  WeatherRealm.swift
//  SKBWeather
//
//  Created by Sergey Borovikov on 12/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import RealmSwift

class WeatherRealm: Object {
    
    @objc dynamic var id = -1
    @objc dynamic var city = ""
    @objc dynamic var descript = ""
    @objc dynamic var date = Date()
    @objc dynamic var icon = ""
    @objc dynamic var editDate = Date()
    let temp = RealmOptional<Int>()
    let dayDegress = RealmOptional<Int>()
    let nightDegress = RealmOptional<Int>()
    
    override static func primaryKey() -> String? {
        return "city"
    }
}
