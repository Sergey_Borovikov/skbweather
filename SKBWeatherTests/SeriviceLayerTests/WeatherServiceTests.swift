//
//  WeatherServiceTests.swift
//  SKBWeatherTests
//
//  Created by Sergey Borovikov on 13/05/2018.
//  Copyright © 2018 Sergey Borovikov. All rights reserved.
//

import XCTest
@testable import SKBWeather

class WeatherServiceTests: XCTestCase {
    
    private var weatherService: WeatherServiceI!
    private var requests: RequestCoreI!
    private var webClientMock: WebClientCoreI!
    private var mapper: MapperCoreI!
    private var storageMock: StorageCoreI!
    private var notifications: NotificationCoreI!
    
    override func setUp() {
        super.setUp()
        requests = RequestCore()
        webClientMock = WebClientCoreMock()
        mapper = MapperCore()
        storageMock = StorageCore(realm: RealmProvider.realm())
        notifications = NotificationCore()
        weatherService = WeatherService(webClient: webClientMock,
                                        mapper: mapper,
                                        storage: storageMock,
                                        reqests: requests,
                                        notifications: notifications)
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testWeatherInCity() {
        weatherService.weatherInCity("", success: { weatherModel in
            XCTAssert(weatherModel.icon == ._01d)
        }) { _ in }
    }
    
    func testForecastInCity() {
        weatherService.forecastInCity("", success: { forecastModels in
            XCTAssert(forecastModels.first?.icon == ._01d)
        }) { _ in }
    }
}
